 let output = document.getElementById("output");

 let numbers = Array.from(document.getElementsByClassName("number"));
//  console.log(numbers);
 
 let operator = Array.from(document.getElementsByClassName("operator"));
//  console.log(operator);

numbers.map( button => {
    button.addEventListener('click', (e) => {
        switch(e.target.innerText) {
            default:
                output.innerText += e.target.innerText;
        }
    });
} );

operator.map(button => {
    button.addEventListener('click', (e) => {
        switch(e.target.innerText) {
            case 'C':
                output.innerText = "";
                break;
            case '=':
                output.innerText = eval(output.innerText);
                break;
            default:
                output.innerText += e.target.innerText;
        }
    });
});